<?php
ob_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.11.17
 * Time: 14:20
 */
require 'PHPExcel.php';

class XlsxParser
{

    public function __construct()
    {
        if (empty($_FILES))
        {
            throw new Exception('File not uploaded');
        }

        $this->parseFile();
    }

    private function parseFile()
    {
        $filepath = $_FILES['xlsx']['tmp_name'];
        $objPHPExcel = PHPExcel_IOFactory::load($filepath);
        $sheet = $objPHPExcel->getSheet(0);
        $data = $this->fetchArrayFromXlsx($sheet);

        $this->formatArray($data);
    }

    /**
     * Fetches an array from given Xlsx sheet
     * @param $sheet
     * @return array
     */
    private function fetchArrayFromXlsx($sheet)
    {
        $return = array();
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++){
            $rowData = $sheet->rangeToArray(
                'A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE
            );
            $return[$row] = $rowData[0];
        }

        return $return;
    }

    private function toJson($data)
    {
        file_put_contents('data.json', json_encode($data));
        header('Location: ./');
        die();
    }

    private function formatArray($data)
    {
        $array = [];
        $i = 0;

        foreach ($data as $row)
        {
            if (!$row[0] && !$row[9])
            {
                continue;
            }
            $bg = '';
            if (strstr($row[17], '#') && strlen(trim($row[17])) == 7)
            {
                $bg = 'color';
            }
            else
            {
                $bg = 'url';
            }

            if ($row[15]==='title' && $i++ == 0)
            {
                $array = [
                    'title' => [
                        'media' =>
                        [
                            'url' => $row[11],
                            'caption' => $row[13],
                            'credit'  => $row[12],
                            'thumbnail' => $row[14],
                        ],
                        'text' => [
                            'headline' => $row[9],
                            'text' => $row[10],
                        ],
                        'background' => [
                            $bg => $row[17],
                        ],
                    ],
                    'events' => [],
                ];
                continue;
            }

            $array['events'][] = [
                'media' => [
                    'url' => $row[11],
                    'caption' => $row[13],
                    'credit'  => $row[12],
                    'thumbnail' => $row[14],
                ],
                'start_date' => [
                    'year'  => $row[0],
                    'month' => $row[1],
                    'day'   => $row[2],
                    'time'  => $row[3],
                ],
                'end_date' => [
                    'year'  => $row[4],
                    'month' => $row[5],
                    'day'   => $row[6],
                    'time'  => $row[7],
                ],
                'text' => [
                    'headline' => $row[9],
                    'text' => $row[10],
                ],
                'background' => [
                    $bg => $row[17],
                ],
            ];


        }
        return $this->toJson($array);
    }

}

$parser = new XlsxParser();




