<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TimelineJS</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link title="timeline-styles" rel="stylesheet" href="css/font.css">
    <link title="timeline-styles" rel="stylesheet" href="css/timeline.css">
<!--    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700|PT+Serif|PT+Sans&amp;subset=latin-ext" rel="stylesheet">-->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <style>
    </style>
    <![endif]-->
</head>
<body>
<div id='timeline-embed' style="width: 100%; height: 750px"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.knightlab.com/libs/timeline3/latest/js/timeline.js"></script>
<script type="text/javascript">
    // The TL.Timeline constructor takes at least two arguments:
    // the id of the Timeline container (no '#'), and
    // the URL to your JSON data file or Google spreadsheet.
    // the id must refer to an element "above" this code,
    // and the element must have CSS styling to give it width and height
    // optionally, a third argument with configuration options can be passed.
    // See below for more about options.
//    window.timeline = new TL.Timeline('timeline-embed', 'timeline3.json');
    timeline = new TL.Timeline('timeline-embed',
        'data.json', { hash_bookmark: false});
</script>
</body>
</html>